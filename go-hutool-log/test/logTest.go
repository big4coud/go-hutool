package main

/**
 * 日志打印测试用例
 * @author Lucky
 *
 */
import (
	"gitee.com/huangbosbos/go-hutool/go-hutool-log/main/cn/gohutool/log"
)

func main() {
	log.INFO.Print("log message")
	log.ERROR.Print("log message")
	log.WARNING.Print("log message")
	log.FATAL.Print("log message")

}
